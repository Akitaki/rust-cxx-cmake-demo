cmake_minimum_required(VERSION 3.10)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)


project(foo VERSION 1.0.0 DESCRIPTION "Foo library")
add_library(foo STATIC src/foo.cpp)
set_property(TARGET foo PROPERTY CXX_STANDARD 14)
set_target_properties(foo PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(foo PROPERTIES PUBLIC_HEADER include/foo.hpp)
target_include_directories(foo PRIVATE include)

find_package(Torch REQUIRED PATHS $ENV{LIBTORCH})
target_link_libraries(foo "${TORCH_LIBRARIES}")

install (TARGETS foo DESTINATION .)
