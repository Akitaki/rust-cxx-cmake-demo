#include "rust-cxx-playground/include/foo.hpp"

std::unique_ptr<Foo> new_foo() {
    return std::make_unique<Foo>();
}
