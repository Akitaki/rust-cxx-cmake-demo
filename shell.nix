{ pkgs ? (import <nixpkgs> {
    # Rust overlay
    overlays = [
      (import (builtins.fetchTarball {
        url = https://github.com/oxalica/rust-overlay/archive/master.tar.gz;
      }))
    ];
    config.allowUnfree = true;
  })
, lib ? pkgs.stdenv.lib
}:

let
  unstable = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz") {
    config.allowUnfree = true;
  };
in
pkgs.mkShell rec {
  buildInputs = with pkgs; [
    # rust
    unstable.rust-analyzer
    (rust-bin.stable.latest.default.override {
      extensions = [ "rust-src" ];
    })

    gcc
    cmake

    # optional
    llvmPackages.lld
    sccache

    unstable.libtorch-bin
  ];

  # optional
  # RUSTFLAGS = "-C link-arg=-fuse-ld=lld";
  # optional
  RUSTC_WRAPPER = "${pkgs.sccache}/bin/sccache";

  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.stdenv.cc.cc.lib}/lib/:$LD_LIBRARY_PATH
    export LIBTORCHLIB=${unstable.libtorch-bin}
    export LIBTORCH=${unstable.libtorch-bin.dev}
  '';
}
