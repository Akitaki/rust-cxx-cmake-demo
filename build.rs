use std::env;
use std::path::PathBuf;

fn main() {
    let dst = cmake::build("foo");

    eprintln!("dst = {}", dst.display());
    println!("cargo:rustc-link-search=native={}", dst.display());
    println!("cargo:rustc-link-lib=static=foo");

    cxx_build::bridge("src/main.rs")
        .flag_if_supported("-std=c++14")
        .file("src/foo.cpp")
        .compile("rust-cxx-playground");

    let tch_libpath = PathBuf::from(env::var("LIBTORCHLIB").unwrap());
    println!(
        "cargo:rustc-link-search=native={}",
        tch_libpath.join("lib").display()
    );
    println!("cargo:rustc-link-lib=torch");
    println!("cargo:rustc-link-lib=torch_cpu");
    println!("cargo:rustc-link-lib=c10");
}
